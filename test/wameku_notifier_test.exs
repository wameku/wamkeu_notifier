defmodule Foo do
 @behaviour WamekuNotifier

 def notify(message, config) do
   IO.inspect message
   IO.inspect config
 end
end

defmodule WamekuNotifierTest do
  use ExUnit.Case

  test "foo implements notify" do
    assert Foo.notify(%{baz: 1}, [url: "example.com"])
  end
end

# WamekuNotifier

This library describes the basic behaviour a notifier should have.

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add wameku_notifier to your list of dependencies in `mix.exs`:

        def deps do
          [{:wameku_notifier, "~> 0.0.1"}]
        end

  2. Ensure wameku_notifier is started before your application:

        def application do
          [applications: [:wameku_notifier]]
        end


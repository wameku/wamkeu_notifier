defmodule WamekuNotifier do
  @callback notify(Map.t, Map.t) :: any
end
